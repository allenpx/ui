var _ = require('underscore');
var fs = require('fs');
var path = require('path');
var modules = {};

module.exports = function(expressApp){

	fs.readdirSync(__dirname).forEach(function(file) {
		if(path.extname(file) == ".js" && file != 'index.js'){
			modules[file.split(".")[0]] = require(__dirname+"/" + file);
		}
	});

	_.each(modules, function(module, moduleName){
		exports[moduleName] = module.initialize(expressApp);
	},this);

	return this;

}
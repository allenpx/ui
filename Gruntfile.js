
module.exports = function(grunt) {

    var liveReloadPort = 69842;

    grunt.initConfig({
        projectName : 'ui',
        liveReloadPort : liveReloadPort,

        uglify: {
            js:{
                options: {
                    compress:true,
                    mangle:false,
                    sourceMap:false
                },
                files: {
                    '<%= projectName %>.standalone.js': ['<%= projectName %>.standalone.dev.js']
                }
            },
        },

        less: {
            dev: {
                options: {
                    paths: [
                        "less",
                        "node_modules",
                    ],
                    compress: true,
                    sourceMap:true,
                    strictImports:true,
                },
                files: {
                    "css/ui.base.dev.css":  "less/build/ui.base.less",
                    "css/ui.responsive.dev.css":  "less/build/ui.responsive.less",
                }
            },
            prd: {
                options: {
                    paths: [
                        "less",
                        "node_modules",
                    ],
                    compress: true,
                    sourceMap:false,
                    strictImports:true,
                },
                files: {
                    "css/ui.base.css":  "less/build/ui.base.less",
                    "css/ui.responsive.css":  "less/build/ui.responsive.less",
                }
            }

        },


        'template-module': {
            templates:{
                files: "lib/hbs/**/*.hbs",
                options: {
                    provider:'handlebars',
                    module: true,
                    single:true,
                }
            }
        },

        filelist:{
            ui:{
                src: [
                    'less/modules/**/*.less',
                ],
                dest: 'less/config/ui.modules.less',
                transform:'@import "{{filepath}}";'
            },
        },

        browserify: {
          dist: {
            files: {
              'test/test.js': ['lib/test.js'],
            },
            options: {
              //transform: ['coffeeify']
            }
          }
        },


        nodemon: {
          dev: {
            script: 'test/index.js'
          }
        },


        watch:{
            // server:{
            //     files:['test/**/*.js', 'ui.js', 'ui.css'],
            //     options: {
            //       livereload: true
            //     }
            // },
            html: {
                files: ['*.html'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            less: { 
                files: ['less/**/*.less'],
                tasks: ['filelist','less'],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
            templates: {
                files: ['lib/hbs/**/*.hbs'],
                tasks: [
                'template-module',
                'browserify',
                //'uglify'
                ],
                options: {
                  nospawn: true,
                  interrupt: true,
                  livereload:{
                        port:liveReloadPort
                    }
                },

            },
            js: {
                files: ['lib/**/*.js'],
                tasks: [
                'template-module',
                'browserify', 
                //'uglify'
                ],
                options:{
                    livereload:{
                        port:liveReloadPort
                    }
                }
            },
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-template-module');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concat-deps');
    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-tasks');

    grunt.registerTask('default', [
        'template-module',
        'filelist',
        //'concat_deps',
        'browserify',
        'less',
        //'uglify',
        'watch',
        //'nodemon'
    ]);

};

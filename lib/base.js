var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');
var Model = require('./model');
require('backbone-super');


var Base = Backbone.View.extend({

	initialize:function(options){

		if(options.el){
			_.extend(options, this.fromDom(options.el));
		}

		this.options = _.extend({}, {
			'classes':''
		}, options);

		this.id = this.options.id || _.uniqueId('ui');
		this.$el.attr('id', this.id);
		if(options.static != true){
			this.$el.attr('data-ui-init', '');
		}
		this.$el.addClass(this.options.classes);

		if(this.options.static != true){
			ui.registerComponent(this);
		}


		this.model = new Model(this.options.modelOptions || {});
		this.model.set('value', this.options.value || this.options.defaultValue || '');
		this.model.on('change:value', this.applyModelValueToDisplay, this);
		this.model.on('validate:value', this.validated, this);

		this.on('rendered', this.rendered, this);

		this.setBindedModel();

	},

	rendered:function(){
		//this.setDisplayValue();
	},

	setBindedModel:function(){
		if(this.options.model && this.options.attribute){
			this.model.listenTo(this.options.model, 'change:'+this.options.attribute, _.bind(this.applyBindedModelValueToModel, this));
			this.model.listenTo(this.options.model, 'validate:'+this.options.attribute, _.bind(this.validated, this));
			this.applyBindedModelValueToModel();
		}else{
			this.model.set('value', this.options.value || this.options.defaultValue || "");
		}
	},


	applyModelValueToBindedModel:function(event){
		this.options.model.set(this.options.attribute, this.model.get('value'));
	},
	applyBindedModelValueToModel:function(event){
		this.model.set('value', this.options.model.get(this.options.attribute));
	},
	applyDisplayValueToModel:function(event){
		this.model.set('value', this.getDisplayValue());
	},
	applyModelValueToDisplay:function(){
		val = this.model.get('value');
		this.setDisplayValue(val);

		if(this.options.model && this.options.attribute){
			this.applyModelValueToBindedModel();
		}

	},

	error:function(arg){

		if(typeof arg === "boolean"){
			if(arg){
				this._setError(true);
			}else{
				this._setError(false);
			}
		}

		if(typeof arg === "string"){
			this._setError(true, arg);
		}

	},

	/*
	
  ,ad8888ba,                                                88          88
 d8"'    `"8b                                               ""          88
d8'        `8b                                                          88
88          88 8b       d8  ,adPPYba, 8b,dPPYba, 8b,dPPYba, 88  ,adPPYb,88  ,adPPYba,
88          88 `8b     d8' a8P_____88 88P'   "Y8 88P'   "Y8 88 a8"    `Y88 a8P_____88
Y8,        ,8P  `8b   d8'  8PP""""""" 88         88         88 8b       88 8PP"""""""
 Y8a.    .a8P    `8b,d8'   "8b,   ,aa 88         88         88 "8a,   ,d88 "8b,   ,aa
  `"Y8888Y"'       "8"      `"Ybbd8"' 88         88         88  `"8bbdP"Y8  `"Ybbd8"'


	 */
	getDisplayValue:function(){
		return true;
	},
	setDisplayValue:function(value){

	},
	listenToDisplay:function(){

	},
	validated:function(validation){

	},

	_setError:function(isError, message){

	},

	applyDataAttributes:function(options, element){
		options = options || {};
		element = element || this.$el;

		_.each(options, function(value, key){
			if(key.substr(0,5) == "data-"){
				element.attr(key, value);
			}
		});

	},

	dataAttributeMapping:{},

	mapDataAttributes:function(options, mapping){
		if(!mapping){
			mapping = Base.dataAttributeMapping;
		}
		options = options || {};
		var valueMap;
		var keyMap;
		var type;
		_.each(options, function(value, key){

			if(key.substr(0,5) == "data-" && typeof mapping[key.substr(5)] != 'undefined'){
				keyMap = key.substr(5);
				valueMap = mapping[key.substr(5)];
				type = "string";

				if(_.isObject(valueMap)){
					if(valueMap.type == "boolean"){
						value = _.toBoolean(value);
					}
				}
				options[keyMap] = value;
			}
		});

		return options;

	},

	getOptionsFromDom:function(options){
		Base.mapDataAttributes(options);

		return options;
	},
	
	fromDom:function(element){
			
		var options = {};

		if(element.find('.error-message')){
			options.errorMessage = element.find('.error-message').html();
		}

		return options;

	},

	destroy:function(){
		ui.trigger('destroy', this);
	},

});

module.exports = Base;
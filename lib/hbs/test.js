var _ = require('handlebars/runtime');
module.exports = _.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "\r\n"
    + escapeExpression(((helpers['ui-textfield'] || (depth0 && depth0['ui-textfield']) || helperMissing).call(depth0, {"name":"ui-textfield","hash":{
    'data-iconleft': ("fa fw fa-rocket"),
    'data-validate': ("email"),
    'data-placeholder': ("Email"),
    'data-value': (""),
    'data-error': ("Please enter your email."),
    'data-name': ("testname"),
    'data-autocomplete': ("off")
  },"data":data})));
},"useData":true});
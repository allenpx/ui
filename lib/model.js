var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');
require('backbone-super');

var Model = Backbone.Model.extend({

	initialize:function(options){

		this.options = options || {};

		this.validation = _.extend({}, this.options.validation);

		this.on('change', this.change, this);

	},

	change:function(){

		this.validate();

	},
	validate:function(){
		this.isValid();
	},
	isValid:function(args){

		var attributesToCheck = [];

		switch(Object.prototype.toString.call(args)){
			case '[object Array]':
				attributesToCheck = args;
				break;
			case '[object String]':
				attributesToCheck = [args];
				break;
			case '[object Undefined]':
				attributesToCheck = _.keys(this.validation);
				break;
		}

		var modelValidation = {
			valid:true,
			invalid:[]
		};
		var invalidProperties = [];

		_.each(attributesToCheck, function(key){
			var validationOptions = this.validation[key];
			var validated = this.validateAttribute(key, validationOptions);
			if(validated.valid == false){
				modelValidation.invalid.push(validated);
			}
		},this);

		if(modelValidation.invalid.length){
			modelValidation.valid = false;
		}

		this.trigger("validate", modelValidation);

	},

	validateAttribute:function(key, options){

		var attribute = this.validation[key];
		attribute.validation = {valid:true, message:''};

		var isEmpty = ui.Model.isEmpty(this.get(key)).valid;

		if(attribute.validation.valid && options.required && isEmpty == true){
			this.validation[key].validation = ui.Model.isEmpty(this.get(key));
		}

		if(attribute.validation.valid && options.pattern && isEmpty == false){
			this.validation[key].validation = ui.Model.isEmail(this.get(key));
		}

		attribute.validation.attribute = key;

		this.trigger("validate:"+key, {model:this, validation:attribute.validation});

		return attribute.validation;

	},

},{

	isEmpty:function(value){
		var v = (value.trim(value).length)?false:true;

		if(v){
			return {
				valid:true
			}
		}else{
			return {
				valid:false,
				message:ui.Model.messages.isEmail
			}
		}

		return ui.Model.returnValidation(v, ui.Model.messages.isEmpty);
	},

	isEmail:function(value){
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var v = re.test(value);

		return ui.Model.returnValidation(v, ui.Model.messages.isEmail);
	},

	returnValidation:function(isValid, message){
		if(isValid){
			return {
				valid:true
			}
		}else{
			return {
				valid:false,
				message:message
			}
		}
	},

	messages:{
		isEmpty:"Must not be empty",
		isEmail:"Must be a valid email."
	}


});

module.exports = Model;
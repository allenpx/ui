var Base = require('./base');
var ui = require('./ui');
var $ = require('cheerio');
var _ = require('underscore');
var Handlebars = require('handlebars/runtime');

var Textfield = Base.extend({

	initialize:function(options){

		this._super(_.extend({
			defaultValue:""
		}, options));
		
		this.$errorMessage = $('<div class="error-message"></div>');

		if(this.options.input != 'textarea'){
			this.$input = $('<input type="text"/>');
		}else{ 
			this.$input = $('<textarea></textarea>');
		}

		this.$input.attr('id', _.uniqueId('input'));

		if(this.options.name){
			this.$input.attr('name', this.options.name);
		}

        if(this.options.readonly){
            this.$input.attr('readonly', true);
        }

		if(this.options.input){
			this.$input.attr('type', this.options.input);
		}
		if(this.options.placeholder){
			this.$input.attr('placeholder', this.options.placeholder);
		}

		if(this.options.autocomplete){
			this.$input.attr('autocomplete', this.options.autocomplete);
		}

		if(this.options.error){
			this.options.errorValue = this.options.value;
		}

        if(this.options.autoselect){
            this.$input.attr('onClick', 'this.setSelectionRange(0, this.value.length)');
        }

		this.$label = $('<label ></label>');
		this.$label.attr('for', this.$input.attr('id'));

	},

	render:function(){

		this.$el.empty();
		this.$el.addClass('ui textfield');
		this.$el.addClass(this.options.classes);
		if(this.options.static){
			this.applyDataAttributes(this.options);
		}
		var classes = [];

		this.$label.text(this.options.label);

		this.$el.empty();
		if(this.options.label){
			this.$el.append(this.$label);
		}
		this.$el.append(this.$input);

		if(this.options.iconleft){
			classes.push('icon-left');
			var iconLeft = $('<div class="accessory accessory-left accessory-icon"><i class="'+this.options.iconleft+'"></i></div>');
			this.attachAccessory(iconLeft);
			this.$input.before(iconLeft);
		}

		if(this.options.iconright){
			classes.push('icon-right');
			var iconRight = $('<div class="accessory accessory-right accessory-icon"><i class="'+this.options.iconright+'"></i></div>');
			this.attachAccessory(iconRight);
			this.$input.after(iconRight);
		}

		if(this.options.labelleft){
			classes.push('icon-left');
			var labelLeft = $('<div class="label accessory accessory-left accessory-label">'+this.options.labelleft+'</div>');
			this.attachAccessory(labelLeft);
			this.$input.before(labelLeft);
		}
		if(this.options.labelright){
			classes.push('icon-right');
			var labelRight = $('<div class="label accessory accessory-right accessory-label">'+this.options.labelright+'</div>');
			this.attachAccessory(labelRight);
			this.$input.after(labelRight);
		}

		if( (this.options.errorMessage || this.options.errormessage) && this.options.errorMessage ){
			this.error(this.options.errorMessage);
		}

		this.$el.addClass(classes.join(' '));
		this.applyModelValueToDisplay();

		this.trigger('rendered')

		return this;
	},
 
	rendered:function(){
		this._super();
		this.listenToDisplay();
		if(this.options.error){
			this.error(this.options.error);
		}
	},

	setDisplayValue:function(value){
		if(this.$input){
			this.$input.val(value);
			this.$input.attr('value',value);
		}
	},
	getDisplayValue:function(){
		return this.$input.val();
	},

	listenToDisplay:function(){

		if(this.options.static){
			return false;
		}
		$('body').on('change keyup', '#'+this.$input.attr('id'), $.proxy(function(event){
			this.applyDisplayValueToModel();
		}, this));
		$('body').on('focusin', '#'+this.$input.attr('id'), $.proxy(this.focusin, this));
		$('body').on('focusout', '#'+this.$input.attr('id'), $.proxy(this.focusout, this));

		if(this.options.input == "textarea"){
			Textfield.watchTextarea(this);
		}

	},
	
	
	fromDom:function(element){
		
		var options = this._super(element);

		if(element.find('label, .label').length){
			options.label = element.find('label').text();
		}
		if(element.find('input').length){
			var $input = element.find('input');
			options.value = $input.val() || "";
			options.placeholder = $input.attr('placeholder');
		}

		return options;

	},

	validated:function(event){
 		
		if(!event.validation.valid){
			this.error(event.validation.message)
		}else{
			this.error(false);
		}

	},

	_setError:function(isError, message){

		if(this.options.error && this.model.get('value') == this.options.errorValue){
			this.$el.addClass('error');
			if(this.$errorMessage){
				this.$errorMessage.html(this.options.error);
				this.$el.append(this.$errorMessage);
			}
			return false;
		}else{
			this.options.error = null;
		}

		if(isError){
			this.$el.addClass('error');
			if(this.$errorMessage){
				this.$errorMessage.html(message);
				this.$el.append(this.$errorMessage);
			}
		}else{
			this.$el.removeClass('error');
			if(this.$errorMessage){
				this.$errorMessage.remove();
			}
		}
	},

	focus:function(event){
		this.$input.focus();
	},
	focusin:function(){
		this.$el.addClass('focus');
	},

	focusout:function(event){
		this.$el.removeClass('focus');
	},

	attachAccessory:function($element){
		if(process.browser){
			var id = _.uniqueId('acc');
			$element.attr('id', id);

			$('body').on('click', '#'+id, $.proxy(this.focus, this));
		}
	},



}, {

	optionsFromDom:function(element){
		var $element = $(element);
		var options = $element.data();
		options.el = $element;

		if(options.validate){
			options.modelOptions = {
				validation:{
					"value":{
						pattern:options.validate
					}
				}
			}
		}

		var newComponent = new ui.Textfield(options).render();
		newComponent.model.validate();
	},


	dataAttributeMapping:_.extend({}, Base.dataAttributeMapping, {
		'iconleft':"string",
		'iconright':"string",
		'validate':"string",
		'value':"string",
		'placeholder':"string",
		'name':"string",
		'error':"string",
		'autocomplete':"string",
	}),

	getOptionsFromDom:function(options){
		options = Textfield.__super__.mapDataAttributes(options, Textfield.dataAttributeMapping);

		if(options['data-validate']){
			options.modelOptions = {
				validation:{
					"value":{
						pattern:options.validate
					}
				}
			}
		}

		return options; 
	},

	renderHelper:function(data){
		data.hash.static = true;
		var element = new Textfield(Textfield.getOptionsFromDom(data.hash)).render();
		return new Handlebars.SafeString(ui.html(element.$el));
	},

	watchTextarea:function(self) {

		self.$input.bind('keyup keydown change', function(event){

			Textfield.expandTextarea(self.$input[0]);
			self.$input.scrollTop(0);

			if(self.options.linebreaks == false){
				if(event.which == '13') {
				      return false;
				}
			}

		});

		self.$input.bind('paste', function(event){

			Textfield.expandTextarea(self.$input[0]);
			self.$input.scrollTop(0);

		});

		_.delay(function(){
			Textfield.expandTextarea(self.$input[0]);
		},50);

	},
	expandTextarea:function(element){
		element.style.overflow = 'hidden';
		element.style.height = 0;
		element.style.height = element.scrollHeight + 'px';
	}

});

ui.registerSelector('.ui.textfield', _.bind(Textfield.optionsFromDom, this));
ui.registerHelper('ui-textfield', _.bind(Textfield.renderHelper, this)); 

module.exports = Textfield;







var $ = require('cheerio');
var _ = require('underscore');
var Handlebars = require('handlebars/runtime');
global.Handlebars = Handlebars;
var Backbone = require('backbone');
Backbone.$ = $;
Backbone._ = _;
require('backbone-super');
var jQuery = $;
var events = require('inherit/events');


_.mixin({
	toBoolean:function(value){
		if(value.toLowerCase() === "true"){
			return true;
		}else{
			return false;
		}
	}
})

var UI = events.extend({

	initialize:function(options){

		options = options || {};
		this.options = _.extend({
			exportGlobal:false
		}, options);

		this.on('destroy', this.destroy, this);

	}, 

	render:function(domScope){

		var self = this;

		$domScope = domScope || $('body');

		$domScope.find('*[data-ui]').not('data-ui-init').each(function(index, element){
			var $el = $(element);

			if(self[$el.data('ui')]){

				var data = $el.data();
				if(data.validate){
					data.modelOptions = {
						validation:{
							"value":{
								pattern:data.validate
							}
						}
					}
				}

				var uiElement = new self[$el.attr('data-ui')](data).render();
				$el.replaceWith(uiElement.$el);
			}
		});

		_.each(this.selectors, function(selectorFunc, selectorStr){
			$domScope.find(selectorStr).not('[data-ui-init]').each(function(i,element){
				selectorFunc(element);
			});
		},this);

	},


	elements:{},

	get:function(id){
		return this.elements[id];
	},

	registerComponent:function(component){
		this.elements[component.id] = component;
	},

	destroy:function(element){
		delete this.elements[element.id];
	},

	selectors:{},
	registerSelector:function(selector, callback){
		this.selectors[selector] = callback;
	},

	helpers:{},
	registerHelper:function(selector, helperFunc){
		this.helpers[selector] = helperFunc;
	},
	registerHelpers:function(handlebarsInstance){
		if(typeof handlebarsInstance == "undefined"){
			handlebarsInstance = Handlebars;
		}
		_.each(this.helpers, function(helperFunc, selector){
			handlebarsInstance.registerHelper(selector, helperFunc);
		});
	},
	html:function($element){
		if(process.browser){
			return $element[0].outerHTML;
		}else{
			return $.html($element);
		}
	}


});

var ui = new UI();

module.exports = ui;


ui.Base = require('./base');
ui.Dialog = require('./dialog');
ui.Layout = require('./layout');
ui.Model = require('./model');
ui.Textfield = require('./textfield');
ui.Toggle = require('./toggle');
ui.Form = require('./form');

if(process.browser){
	ui.registerHelpers();
}


var Backbone = require('backbone');
var $ = require('cheerio');
var _ = require('underscore');
var ui = require('./ui');
require('backbone-super');

var Layout = Backbone.View.extend({

	namedElements:null,

 	initialize:function(options){

 		this.options = _.extend({},{
 			classes:'',
 			model:null,
 			elements:[]
 		}, options || {});

 		this.namedElements = {};

 	},

	build:function(){
		this.$el.empty();

		this.$el.append(this.buildFragment(this.options.elements, this.$el));
	},

	buildFragment:function(childrenArray, $parentElement){

		if(Object.prototype.toString.call(childrenArray) === '[object Object]'){
			childrenArray = [childrenArray];
		}

		_.each(childrenArray, function(child){

			if(this.options.model != null && typeof child.model == 'undefined'){
				child.model = this.options.model;
			}

			child.tagName = child.tagName || "div";
			child.type = child.type || '';
			if(child.type.indexOf('ui.') != -1){
				child.options = child.options || {};
				var $item = new ui[child.type.split('.')[1]](child).render();
				$item.$el.attr('id', child.name);
			}else if(child.type == "html"){
				var $item = $(child.html);
				$item.attr('id', child.name);
				$item.addClass(child.type);
			}else{
				var $item = $('<'+child.tagName+'/>');
				$item.attr('id', child.name);
				$item.addClass(child.type);
			}

			if(child.name){
				this.namedElements[child.name] = $item;
			}else{
				child.name = _.uniqueId("e");
			}
			

			if(child.events){
				_.each(child.events, function(eventObject){
					$body.on(eventObject.event, '#'+child.name, eventObject.callback);
				},this );
			}

			if(child.classes){
				$item.addClass(child.classes);
			}
			if(child.text){
				$item.text(child.text);
			}

			if(child.iconLeft){
				$item.prepend('<i class="'+child.iconLeft+'"></i>&nbsp;');
			}

			if(child.elements != undefined){ 
				if( _.keys(child.elements).length ){
					$item.append(this.buildFragment(child.elements, $item));
				}
			}

			if($item.$el != undefined){
				$parentElement.append($item.$el);
			}else{
				$parentElement.append($item);
			}

		},this);

	},

 	get:function(id){
 		return this.namedElements[id];
 	},

 	render:function(){
 		this.$el.addClass(this.options.classes);
 		this.build();
 		return this;
 	}

});

module.exports = Layout;

var Base = require('./base');
var $ = require('jquery');
var ui = require('./ui');
var _ = require('underscore');


var Toggle = Base.extend({

	initialize:function(options){

		this._super(_.extend({
			label:"Toggle"
		}, options));

		this.$checkbox = $('<input type="checkbox"/>');
		this.$checkbox.attr('id', _.uniqueId('input'));
		this.$label = $('<label ></label>');
		this.$label.attr('for', this.$checkbox.attr('id'));




	},

	render:function(){

		this.$el.addClass('ui toggle');
		this.$el.addClass(this.options.classes);

		this.$label.text(this.options.label);

		this.$el.empty();
		this.$el.append(this.$checkbox);
		this.$el.append(this.$label);

		this.applyModelValueToDisplay();

		this.rendered();

		return this;
	},

	rendered:function(){
		this.listenToDisplay();
	},

	setDisplayValue:function(value){
		if(this.$checkbox){
			this.$checkbox.prop('checked', value);
		}
	},
	getDisplayValue:function(){
		return this.$checkbox.prop('checked');
	},

	listenToDisplay:function(){

		$('body').on('change', '#'+this.$checkbox.attr('id'), $.proxy(this.applyDisplayValueToModel, this));

	},
	
	
	fromDom:function(element){
		
		var options = {};

		if(element.find('label, .label')){
			options.label = element.find('label').text();
		}

		if(element.find('input')){
			options.value = element.find('input').prop('checked');
		}

		return options;

	}

});

module.exports = Toggle;